// rn2903.h
// Controls sending and receiving from the RN2903 Microchip LoRa Module.
// There are 2 different radio modes: LoRaWAN and LoRa.
// LoRa mode allows access to the low level registers and allows creation of propriatory networks.
// LoRaWAN mode configures the device to communicate to LoRaWAN gateways.
// Author: Bjotech
// Contributors: Bryan Brumm

#ifndef _RN2903_H_
#define _RN2903_H_

#define RN2903_NO_CALLBACK 			(0)
#define RN2903_MAX_BUFFER_SIZE	(128)

typedef enum {
	RN2903_STATUS_SUCCESS,
	RN2903_STATUS_FAILURE,
	RN2903_STATUS_NOT_INIT,
} RN2903_STATUS_TYPE;

void Rn2903_Init(void);

RN2903_STATUS_TYPE Rn2903_Process(void);

RN2903_STATUS_TYPE Rn2903_SendCommand(char* data, unsigned short length);

RN2903_STATUS_TYPE Rn2903_SetupRxCallback(void(*rn2903_rxCallback)(char* data, unsigned short length));

#endif
