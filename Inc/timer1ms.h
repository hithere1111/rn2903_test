// timer1ms.h
// Controls a timer based interrupt for 1ms timer variables.
// Interrupt is called every 1ms, and variables defined in the header are incremented 1.
// Note that these variables are approximate in ms. They will be off by a few microcseconds.
// Author: Bjotech
// Contributors: Bryan Brumm

#ifndef _TIMER1MS_H_
#define _TIMER1MS_H_

#include "tim.h"

typedef enum {
	TIMER1MS_STATUS_SUCCESS,
	TIMER1MS_STATUS_FAILURE,
} TIMER1MS_STATUS_TYPE;

typedef enum {
	TIMER1MS_TIMER_HEARTBEAT,
	TIMER1MS_TIMER_RN2903_RX_WAIT,
	TIMER1MS_TIMER_CONSOLE,
	NUMBER_OF_TIMER1MS_TIMERS
} TIMER1MS_TIMER_TYPE;

TIMER1MS_STATUS_TYPE Timer1ms_Init(TIM_HandleTypeDef* htim);

unsigned long Timer1ms_Get(TIMER1MS_TIMER_TYPE timer);

TIMER1MS_STATUS_TYPE Timer1ms_Set(TIMER1MS_TIMER_TYPE timer, unsigned long time);

void Timer1ms_Update(void); /* only call by interrupt service routine */

#endif
