// rn2903_test.h
// Provides test functions for rn2903 and shows how to make
// calls to the rn2903 module.
// Author: Bjotech
// Contributors: Bryan Brumm

#ifndef _RN2903_TEST_H_
#define _RN2903_TEST_H_

typedef enum {
	RN2903_T_STATUS_SUCCESS,
	RN2903_T_STATUS_FAILURE,
	RN2903_T_STATUS_TEST_IN_PROGRESS,
} RN2903_T_STATUS_TYPE;

void Rn2903Test_Init(void);

RN2903_T_STATUS_TYPE Rn2903Test_Process(void);

void Rn2903Test_PrintResults(void);

#endif
