// console.h
// Provides functions for printing data nicely to terminal console throuhg USB.
// Author: Bjotech
// Contributors: Bryan Brumm

#ifndef _CONSOLE_H_
#define _CONSOLE_H_

#define CONSOLE_MAX_TX_BUFFER_SIZE	(512)

typedef enum {
	CONSOLE_STATUS_SUCCESS,
	CONSOLE_STATUS_FAILURE,
	CONSOLE_STATUS_DEBUG_NOT_INIT,
} CONSOLE_STATUS_TYPE;

CONSOLE_STATUS_TYPE Console_Init(void);

CONSOLE_STATUS_TYPE Console_Process(void);

CONSOLE_STATUS_TYPE Console_PrintCommaSeperateDecimal(unsigned char* data, unsigned char length);

CONSOLE_STATUS_TYPE Console_Print(char* data);

CONSOLE_STATUS_TYPE Console_DebugPrintCommaSeperateDecimal(unsigned char* data, unsigned char length);

CONSOLE_STATUS_TYPE Console_DebugPrint(char* data);

#endif
