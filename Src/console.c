// console.c
// Provides functions for printing data nicely to terminal console throuhg USB.
// Author: Bjotech
// Contributors: Bryan Brumm

#include "console.h"
#include "timer1ms.h"
#include "usbd_cdc_if.h"

#define CONSOLE_NOT_INIT	(0)
#define CONSOLE_INIT			(1)

#define CONSOLE_DEBUG_OFF	(0)
#define CONSOLE_DEBUG_ON	(1)

#define CONSOLE_MAX_PRINTABLE_SIZE	(128)
#define CONSOLE_PRINT_TIMEOUT_MS		(1000)

static unsigned char ConsoleInit = CONSOLE_NOT_INIT;
static unsigned char ConsoleDebug = CONSOLE_DEBUG_OFF;

static unsigned char ConsoleTxPacket[CONSOLE_MAX_TX_BUFFER_SIZE];
static unsigned short ConsoleTxPacketLength = 0;

static void console_send(void);

// Console_Init
// Initializes any variables needed.
// Inputs: None
// Outputs: Currently always returns success
CONSOLE_STATUS_TYPE Console_Init(void) {
	if(ConsoleInit == CONSOLE_NOT_INIT) {
		Timer1ms_Init(&htim6);
		Timer1ms_Set(TIMER1MS_TIMER_CONSOLE, 0);
		ConsoleTxPacketLength = 0;
		ConsoleInit = CONSOLE_INIT;
	}
	
	return CONSOLE_STATUS_SUCCESS;
}

// Console_Process
// Every 2 seconds or if data is at max limit,
// sends out packet.
// Inputs: None
// Outputs: Can fail if not initialzed.
CONSOLE_STATUS_TYPE Console_Process(void) {
	if(ConsoleInit == CONSOLE_NOT_INIT) {
		return CONSOLE_STATUS_FAILURE;
	}
	
	if(Timer1ms_Get(TIMER1MS_TIMER_CONSOLE) >= CONSOLE_PRINT_TIMEOUT_MS) {
		console_send();
	}
	
	return CONSOLE_STATUS_SUCCESS;
}

// Console_PrintCommaSeperateDecimal
// Prints Ascii array of comma seperated integers to terminal.
// Cannot have a array of integers larger then 64.
// Inputs: The pointer to array of integers.
// Outputs: Possible failures include console not initialized.
CONSOLE_STATUS_TYPE Console_PrintCommaSeperateDecimal(unsigned char* data, unsigned char length) {
	unsigned char resultingString[CONSOLE_MAX_PRINTABLE_SIZE+1]; // size needs to be bigger than (3*64)+64+1
	unsigned short resultingStringLength = 0;
	unsigned char formattedInteger[5]; // size needs to be bigger then the largest char 255
	unsigned char formattedIntegerLength = 0;
	unsigned char i = 0;

	for(i=0; i<length; i++) {
		formattedIntegerLength = sprintf((char*)formattedInteger, "%d", data[i]);
		
		if(formattedIntegerLength <= 0) {
			continue;
		}
		
		formattedInteger[formattedIntegerLength++] = ',';
		
		memcpy(&resultingString[resultingStringLength], formattedInteger, formattedIntegerLength);
		resultingStringLength += formattedIntegerLength;
	}
	
	// Nothing formatted correctly or it was a length of 0
	if(resultingStringLength == 0) {
		return CONSOLE_STATUS_FAILURE;
	}
	
	// replace final comma with new line
	resultingString[resultingStringLength-1] = '\n';
	
	if( (resultingStringLength+ConsoleTxPacketLength) >= CONSOLE_MAX_TX_BUFFER_SIZE) {
		console_send();
	}
	
	memcpy(&ConsoleTxPacket[ConsoleTxPacketLength], resultingString,  resultingStringLength);
	ConsoleTxPacketLength += resultingStringLength;
	return CONSOLE_STATUS_SUCCESS;
}

// Console_Print
// Prints ascii string to terminal.
// Must have terminating 0.
// Inputs: Pointer to string with terminating 0.
// Outputs: Can fail if not initialized or goes above max printable value.
CONSOLE_STATUS_TYPE Console_Print(char* data) {
	unsigned short i = 0;
	unsigned short resultingStringLength = 0;
	unsigned char resultingString[CONSOLE_MAX_PRINTABLE_SIZE];
	for(i=0; i<CONSOLE_MAX_PRINTABLE_SIZE; i++) {
		if(data[i] == 0) {
			resultingString[resultingStringLength++] = '\n';
			resultingString[resultingStringLength++] = '\r';
			break;
		} else {
			resultingString[resultingStringLength++] = data[i];
		}
	}
	
	if( (resultingStringLength+ConsoleTxPacketLength) >= CONSOLE_MAX_TX_BUFFER_SIZE) {
		console_send();
	}
	
	memcpy(&ConsoleTxPacket[ConsoleTxPacketLength], resultingString,  resultingStringLength);
	ConsoleTxPacketLength += resultingStringLength;
	
	return CONSOLE_STATUS_SUCCESS;
}

// Console_DebugPrintCommaSeperateDecimal
// Checks if print debugging statements is enabled.
// If so, calls Console_PrintCommandSeperateDecimal.
// Cannot have a length of intergers larger then 256.
// Inputs: The pointer to array of integers and length of array.
// Outputs: Possible failures include console not initialized.
CONSOLE_STATUS_TYPE Console_DebugPrintCommaSeperateDecimal(unsigned char* data, unsigned char length) {
	if(ConsoleDebug == CONSOLE_DEBUG_ON) {
		return Console_PrintCommaSeperateDecimal(data, length);
	}
	
	return CONSOLE_STATUS_DEBUG_NOT_INIT;
}

// Console_DebugPrint
// Checks if print debugging statements is enabled.
// If so, prints ascii string to terminal.
// Must have terminating 0.
// Inputs: Pointer to string with terminating 0.
// Outputs: Can fail if not initialized or goes above max printable value.
CONSOLE_STATUS_TYPE Console_DebugPrint(char* data) {
	if(ConsoleDebug == CONSOLE_DEBUG_ON) {
		return Console_Print((char*)data);
	}
	
	return CONSOLE_STATUS_DEBUG_NOT_INIT;
}

// console_send
// Sends data out based on global tx array through usb or uart.
// Inputs: None
// Outputs: None.
static void console_send(void) {
	if(ConsoleTxPacketLength != 0) {
		CDC_Transmit_FS(&ConsoleTxPacket[0], ConsoleTxPacketLength);
	}
	ConsoleTxPacketLength = 0;
	Timer1ms_Set(TIMER1MS_TIMER_CONSOLE, 0);
}
