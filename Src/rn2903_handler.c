// rn2903_handler.c
// Controls state machine for sending/receiving to/from rn2903 module
// for each command. Consists of system, mac, and radio commands.
// Author: Bjotech
// Contributors: Bryan Brumm

#include "console.h"
#include "fifo.h"
#include "rn2903.h"
#include "rn2903_handler.h"
#include "string.h"
#include "timer1ms.h"

#define RN2903_H_NOT_INIT	(0)
#define RN2903_H_INIT			(1)

#define RN2903_H_MAX_COMMAND_BUFFER_SIZE	(16)

#define RN2903_H_HAS_NO_RESPONSE		(0)
#define RN2903_H_DEFAULT_TIMEOUT_MS	(3000)

#define RN2903_H_COMMAND_NOT_ACKNOWLEDGED	(0)
#define RN2903_H_COMMAND_ACKNOWLEDGED			(1)

// Private local variables. //
static char* CommandTypes[RN2903_COMMAND_NUMBER] = {
	"0",
	"sys",
	"mac",
	"radio"
};

static char* SysCommands[RN2903_SYSTEM_COMMAND_NUMBER] = {
	"sleep",
	"reset",
  "eraseFW",
	"factoryRESET",
	"set",
	"get"
};

static char* SysSetCommands[RN2903_SYSTEM_SET_NUMBER] = {
	"nvm",
	"pindig",
	"pinmode"
};

static char* SysGetCommands[RN2903_SYSTEM_GET_NUMBER] = {
	"ver",
	"nvm",
	"vdd",
	"hweui",
	"pindig",
	"pinana"
};

static char* MacCommands[RN2903_MAC_COMMAND_NUMBER] = {
	"reset",
	"tx",
	"join",
	"save",
	"forceENABLE",
	"pause",
	"resume",
	"set",
	"get"
};

static char* MacSetCommands[RN2903_MAC_SET_NUMBER] = {
	"devaddr",
	"deveui",
	"appeui",
	"nwkskey",
	"appskey",
	"appkey",
	"pwridx",
	"dr",
	"adr",
	"bat",
	"retx",
	"linkchk",
	"rxdelay1",
	"ar",
	"rx2",
	"sync",
	"upctr",
	"dnctr",
	"ch"
};

static char* MacGetCommands[RN2903_MAC_GET_NUMBER] = {
	"devaddr",
	"deveui",
	"appeui",
	"dr",
	"pwridx",
	"adr",
	"retx",
	"rxdelay1",
	"rxdelay2",
	"ar",
	"rx2",
	"dcycleps",
	"mrgn",
	"gwnb",
	"status",
	"sync",
	"upctr",
	"dnctr",
	"ch"
};

static char* RadioCommands[RN2903_RADIO_COMMAND_NUMBER] = {
	"rx",
	"tx",
	"cw",
	"set",
	"get"
};

static char* RadioSetCommands[RN2903_RADIO_SET_NUMBER] = {
	"bt",
	"mod",
	"freq",
	"pwr",
	"sf",
	"afcbw",
	"rxbw",
	"bitrate",
	"fdev",
	"prlen",
	"crc",
	"iqi",
	"cr",
	"wdt",
	"sync",
	"bw"
};

static char* RadioGetCommands[RN2903_RADIO_GET_NUMBER] = {
	"bt",
	"mod",
	"freq",
	"pwr",
	"sf",
	"afcbw",
	"rxbw",
	"bitrate",
	"fdev",
	"prlen",
	"crc",
	"iqi",
	"cr",
	"wdt",
	"bw",
	"snr",
	"sync"
};

typedef enum {
	RN2903_H_GET_DATA_STATE_TX,
	RN2903_H_GET_DATA_STATE_WAIT_FOR_RX
} RN2903_H_GET_DATA_STATE_TYPE;
RN2903_H_GET_DATA_STATE_TYPE Rn2903GetDataState = RN2903_H_GET_DATA_STATE_TX;

static struct rn2903_command_type Rn2903CurrentCommand;
static unsigned char Rn2903HandlerInit = RN2903_H_NOT_INIT;
static unsigned char Rn2903AcknowledgementStatus = RN2903_H_COMMAND_NOT_ACKNOWLEDGED;

// Possible responses to receive back from rn2903.
static char Rn2903OkResponse[] = "ok";
static char Rn2903InvalidResponse[] = "Invalid";

// Private local functions. //
static void rn2903_resetCurrentCommand(void);
static void rn2903_handleSystemCommand(void);
static void rn2903_handleMacCommand(void);
static void rn2903_handleRadioCommand(void);
static void rn2903Handler_commandNoArguments(char* preamble, char* cmd, unsigned long timeoutMs);
static void rn2903Handler_receivedCallback(char* data, unsigned short length);
// System set command functions.
static void rn2903_handleSysSetNvm(void);
static void rn2903_handleSysSetPinDig(void);
static void rn2903_handleSysSetPinMode(void);
static void (*rn2903_sysSetFunctions[RN2903_SYSTEM_SET_NUMBER])(void) = { 
	rn2903_handleSysSetNvm, 
	rn2903_handleSysSetPinDig, 
	rn2903_handleSysSetPinMode 
};

// System get command functions.
static void rn2903_handleSysGetVer(void);
static void rn2903_handleSysGetNvm(void);
static void rn2903_handleSysGetVdd(void);
static void rn2903_handleSysGetHweui(void);
static void rn2903_handleSysGetPinDig(void);
static void rn2903_handleSysGetPinAna(void);
static void (*rn2903_sysGetFunctions[RN2903_SYSTEM_GET_NUMBER])(void) = { 
	rn2903_handleSysGetVer, 
	rn2903_handleSysGetNvm, 
	rn2903_handleSysGetVdd, 
	rn2903_handleSysGetHweui, 
	rn2903_handleSysGetPinDig, 
	rn2903_handleSysGetPinAna 
};

// Mac set command functions.
static void rn2903_handleMacSetDevAddr(void);
static void rn2903_handleMacSetDevEui(void);
static void rn2903_handleMacSetAppEui(void);
static void rn2903_handleMacSetNwkSkey(void);
static void rn2903_handleMacSetAppSkey(void);
static void rn2903_handleMacSetPwrIdx(void);
static void rn2903_handleMacSetDr(void);
static void rn2903_handleMacSetAdr(void);
static void rn2903_handleMacSetBat(void);
static void rn2903_handleMacSetRetx(void);
static void rn2903_handleMacSetLinkChk(void);
static void rn2903_handleMacSetRxDelay1(void);
static void rn2903_handleMacSetAr(void);
static void rn2903_handleMacSetRx2(void);
static void rn2903_handleMacSetSync(void);
static void rn2903_handleMacSetUpCtr(void);
static void rn2903_handleMacSetDnCtr(void);
static void rn2903_handleMacSetCh(void);
static void (*rn2903_macSetFunctions[RN2903_MAC_SET_NUMBER])(void) = { 
	rn2903_handleMacSetDevAddr, 
	rn2903_handleMacSetDevEui, 
	rn2903_handleMacSetAppEui, 
	rn2903_handleMacSetNwkSkey, 
	rn2903_handleMacSetAppSkey, 
	rn2903_handleMacSetPwrIdx,
	rn2903_handleMacSetDr,
	rn2903_handleMacSetAdr,
	rn2903_handleMacSetBat,
	rn2903_handleMacSetRetx,
	rn2903_handleMacSetLinkChk,
	rn2903_handleMacSetRxDelay1,
	rn2903_handleMacSetAr,
	rn2903_handleMacSetRx2,
	rn2903_handleMacSetSync,
	rn2903_handleMacSetUpCtr,
	rn2903_handleMacSetDnCtr,
	rn2903_handleMacSetCh
};

// Mac get command functions.
static void rn2903_handleMacGetDevAddr(void);
static void rn2903_handleMacGetDevEui(void);
static void rn2903_handleMacGetAppEui(void);
static void rn2903_handleMacGetDr(void);
static void rn2903_handleMacGetPwrIdx(void);
static void rn2903_handleMacGetAdr(void);
static void rn2903_handleMacGetRetx(void);
static void rn2903_handleMacGetRxDelay1(void);
static void rn2903_handleMacGetRxDelay2(void);
static void rn2903_handleMacGetAr(void);
static void rn2903_handleMacGetRx2(void);
static void rn2903_handleMacGetDCyclePs(void);
static void rn2903_handleMacGetMrgn(void);
static void rn2903_handleMacGetGwNb(void);
static void rn2903_handleMacGetStatus(void);
static void rn2903_handleMacGetSync(void);
static void rn2903_handleMacGetUpCtr(void);
static void rn2903_handleMacGetDnCtr(void);
static void rn2903_handleMacGetCh(void);
static void (*rn2903_macGetFunctions[RN2903_MAC_GET_NUMBER])(void) = { 
	rn2903_handleMacGetDevAddr, 
	rn2903_handleMacGetDevEui, 
	rn2903_handleMacGetAppEui, 
	rn2903_handleMacGetDr, 
	rn2903_handleMacGetPwrIdx, 
	rn2903_handleMacGetAdr,
	rn2903_handleMacGetRetx,
	rn2903_handleMacGetRxDelay1,
	rn2903_handleMacGetRxDelay2,
	rn2903_handleMacGetAr,
	rn2903_handleMacGetRx2,
	rn2903_handleMacGetDCyclePs,
	rn2903_handleMacGetMrgn,
	rn2903_handleMacGetGwNb,
	rn2903_handleMacGetStatus,
	rn2903_handleMacGetSync,
	rn2903_handleMacGetUpCtr,
	rn2903_handleMacGetDnCtr,
	rn2903_handleMacGetCh
};

// Radio get and set command functions
static void rn2903_handleRadioSetBt(void);
static void rn2903_handleRadioSetMod(void);
static void rn2903_handleRadioSetFreq(void);
static void rn2903_handleRadioSetPwr(void);
static void rn2903_handleRadioSetSf(void);
static void rn2903_handleRadioSetAfcBw(void);
static void rn2903_handleRadioSetRxBw(void);
static void rn2903_handleRadioSetBitrate(void);
static void rn2903_handleRadioSetFdev(void);
static void rn2903_handleRadioSetPrLen(void);
static void rn2903_handleRadioSetCrc(void);
static void rn2903_handleRadioSetIqi(void);
static void rn2903_handleRadioSetCr(void);
static void rn2903_handleRadioSetWdt(void);
static void rn2903_handleRadioSetSync(void);
static void rn2903_handleRadioSetBw(void);
static void (*rn2903_radioSetFunctions[RN2903_RADIO_SET_NUMBER])(void) = { 
	rn2903_handleRadioSetBt, 
	rn2903_handleRadioSetMod, 
	rn2903_handleRadioSetFreq, 
	rn2903_handleRadioSetPwr, 
	rn2903_handleRadioSetSf, 
	rn2903_handleRadioSetAfcBw,
	rn2903_handleRadioSetRxBw,
	rn2903_handleRadioSetBitrate,
	rn2903_handleRadioSetFdev,
	rn2903_handleRadioSetPrLen,
	rn2903_handleRadioSetCrc,
	rn2903_handleRadioSetIqi,
	rn2903_handleRadioSetCr,
	rn2903_handleRadioSetWdt,
	rn2903_handleRadioSetSync,
	rn2903_handleRadioSetBw
};

static void rn2903_handleRadioGetBt(void);
static void rn2903_handleRadioGetMod(void);
static void rn2903_handleRadioGetFreq(void);
static void rn2903_handleRadioGetPwr(void);
static void rn2903_handleRadioGetSf(void);
static void rn2903_handleRadioGetAfcBw(void);
static void rn2903_handleRadioGetRxBw(void);
static void rn2903_handleRadioGetBitrate(void);
static void rn2903_handleRadioGetFdev(void);
static void rn2903_handleRadioGetPrLen(void);
static void rn2903_handleRadioGetCrc(void);
static void rn2903_handleRadioGetIqi(void);
static void rn2903_handleRadioGetCr(void);
static void rn2903_handleRadioGetWdt(void);
static void rn2903_handleRadioGetBw(void);
static void rn2903_handleRadioGetSnr(void);
static void rn2903_handleRadioGetSync(void);
static void (*rn2903_radioGetFunctions[RN2903_RADIO_GET_NUMBER])(void) = { 
	rn2903_handleRadioGetBt, 
	rn2903_handleRadioGetMod, 
	rn2903_handleRadioGetFreq, 
	rn2903_handleRadioGetPwr, 
	rn2903_handleRadioGetSf, 
	rn2903_handleRadioGetAfcBw,
	rn2903_handleRadioGetRxBw,
	rn2903_handleRadioGetBitrate,
	rn2903_handleRadioGetFdev,
	rn2903_handleRadioGetPrLen,
	rn2903_handleRadioGetCrc,
	rn2903_handleRadioGetIqi,
	rn2903_handleRadioGetCr,
	rn2903_handleRadioGetWdt,
	rn2903_handleRadioGetBw,
	rn2903_handleRadioGetSnr,
	rn2903_handleRadioGetSync
};
static void rn2903Handler_handleGetCommands(char* data, unsigned char length, unsigned long timeout);
AddPointerFifo(rn2903, RN2903_H_MAX_COMMAND_BUFFER_SIZE, struct rn2903_command_type, RN2903_BUFFER_PASSED, RN2903_BUFFER_FAILED);

// Rn2903Handler_Init
// Ininitializes any buffers needed as well as the rn2903 module.
// Inputs: None
// Outputs: None
void Rn2903Handler_Init(void) {
	if(Rn2903HandlerInit == RN2903_H_NOT_INIT) {
		rn2903Fifo_Init();
		Rn2903_Init();
		Console_Init();
		Rn2903_SetupRxCallback(rn2903Handler_receivedCallback);
		rn2903_resetCurrentCommand();
		Rn2903HandlerInit = RN2903_H_INIT;
	}
}

// Rn2903Handler_Process
// Handles state machine for commands. Updates global command variable
// when set to unknown command and command fifo is not empty.
// Inputs: None
// Outputs: Can fail if not initialized.
RN2903_H_STATUS_TYPE Rn2903Handler_Process(void) {
	if(Rn2903HandlerInit == RN2903_H_NOT_INIT) {
		return RN2903_H_STATUS_FAILURE;
	}
	
	Rn2903_Process();
	
	switch(Rn2903CurrentCommand.commandInfo.commandType) {
		case RN2903_COMMAND_SYSTEM:
		{
			rn2903_handleSystemCommand();
			break;
		}
		
		case RN2903_COMMAND_MAC:
		{
			rn2903_handleMacCommand();
			break;
		}
		
		case RN2903_COMMAND_RADIO:
		{
			rn2903_handleRadioCommand();
			break;
		}
		
		case RN2903_COMMAND_UNKNOWN:
		{
			struct rn2903_command_type rn2903Command;
			if(rn2903Fifo_Get(&rn2903Command) == RN2903_BUFFER_PASSED) {
				Rn2903CurrentCommand = rn2903Command;
			}
			break;
		}
		
		default:
		{
			rn2903_resetCurrentCommand();
			break;
		}
	}
	
	return RN2903_H_STATUS_SUCCESS;
}

RN2903_H_STATUS_TYPE Rn2903Handler_QueueCommand(struct rn2903_command_type command) {
	if(Rn2903HandlerInit == RN2903_H_NOT_INIT) {
		return RN2903_H_STATUS_FAILURE;
	}
	
	if(rn2903Fifo_Put(command) == RN2903_BUFFER_FAILED) {
		return RN2903_H_STATUS_FAILURE;
	}
	
	return RN2903_H_STATUS_SUCCESS;
}

// rn2903_resetCurrentCommand
// Resets the current rn2903 command global.
// Will be called after a failed or successful command.
// Inputs: None
// Outputs: None
static void rn2903_resetCurrentCommand(void) {
	Rn2903CurrentCommand.commandInfo.commandType = RN2903_COMMAND_UNKNOWN;
	Rn2903AcknowledgementStatus = RN2903_H_COMMAND_NOT_ACKNOWLEDGED;
}

// rn2903_handleSystemCommand
// Parses the system commands to find out how to handle
// the command.
// Inputs: None
// Outputs: None
static void rn2903_handleSystemCommand(void) {
	switch(Rn2903CurrentCommand.commandInfo.sysType) {
		case RN2903_SYSTEM_COMMAND_RESET:
	  case RN2903_SYSTEM_COMMAND_FACTORY_RESET:
		{
			// The commands that enter this case do not have arguments.
			// Therefore, just need to have <preamble> <command><cr><lf>
			rn2903Handler_commandNoArguments(	CommandTypes[Rn2903CurrentCommand.commandInfo.commandType], 
																				SysCommands[Rn2903CurrentCommand.commandInfo.sysType] ,
																				RN2903_H_DEFAULT_TIMEOUT_MS);
			break;
		}
		
		case RN2903_SYSTEM_COMMAND_ERASE_FW:
		{
			// Does not have a response, so once sent, wait a predetermined time
			// before resetting timeout timer.
			rn2903Handler_commandNoArguments(	CommandTypes[Rn2903CurrentCommand.commandInfo.commandType], 
																				SysCommands[Rn2903CurrentCommand.commandInfo.sysType] ,
																				RN2903_H_DEFAULT_TIMEOUT_MS);
			break;
		}
		
		case RN2903_SYSTEM_COMMAND_SLEEP:
		{
			break;
		}
		
		case RN2903_SYSTEM_COMMAND_SET:
		{
			if(Rn2903CurrentCommand.commandInfo.sysSetType < RN2903_SYSTEM_SET_NUMBER)
			{ 
				(*rn2903_sysSetFunctions[Rn2903CurrentCommand.commandInfo.sysSetType])();
			} else {
				rn2903_resetCurrentCommand();
			}
			break;
		}
		
		case RN2903_SYSTEM_COMMAND_GET:
		{
			if(Rn2903CurrentCommand.commandInfo.sysGetType < RN2903_SYSTEM_GET_NUMBER)
			{ 
				(*rn2903_sysGetFunctions[Rn2903CurrentCommand.commandInfo.sysGetType])();
			} else {
				rn2903_resetCurrentCommand();
			}
			break;
		}
		
		default:
		{
			rn2903_resetCurrentCommand();
			break;
		}
	}
}

// rn2903_handleMacCommand
// Parses the mac commands to find out how to handle the command.
// Inputs: None
// Outputs: None
static void rn2903_handleMacCommand(void) {
	switch(Rn2903CurrentCommand.commandInfo.macType) {
		case RN2903_MAC_COMMAND_RESET:
		case RN2903_MAC_COMMAND_SAVE:
		case RN2903_MAC_COMMAND_FORCE_ENABLE:
		case RN2903_MAC_COMMAND_PAUSE:
		case RN2903_MAC_COMMAND_RESUME:
		{
			// The commands that enter this case do not have arguments.
			// Therefore, just need to have <preamble> <command><cr><lf>
			break;
		}
		
		case RN2903_MAC_COMMAND_TX:
		{
			break;
		}
		
		case RN2903_MAC_COMMAND_JOIN:
		{
			break;
		}
		
		case RN2903_MAC_COMMAND_SET:
		{
			if(Rn2903CurrentCommand.commandInfo.macSetType < RN2903_MAC_SET_NUMBER)
			{ 
				(*rn2903_macSetFunctions[Rn2903CurrentCommand.commandInfo.macSetType])();
			} else {
				rn2903_resetCurrentCommand();
			}
			break;
		}
		
		case RN2903_MAC_COMMAND_GET:
		{
			if(Rn2903CurrentCommand.commandInfo.macGetType < RN2903_MAC_GET_NUMBER)
			{ 
				(*rn2903_macGetFunctions[Rn2903CurrentCommand.commandInfo.macGetType])();
			} else {
				rn2903_resetCurrentCommand();
			}
			break;
		}
		
		default:
		{
			rn2903_resetCurrentCommand();
			break;
		}
	}
}

// rn2903_handleRadioCommand
// Parses the radio commands to find out how to handle the command.
// Inputs: None
// Outputs: None
static void rn2903_handleRadioCommand(void) {
	switch(Rn2903CurrentCommand.commandInfo.radioType) {
		case RN2903_RADIO_COMMAND_RX:
		{
			break;
		}
		
		case RN2903_RADIO_COMMAND_TX:
		{
			break;
		}
		
		case RN2903_RADIO_COMMAND_CW:
		{
			break;
		}
		
		case RN2903_RADIO_COMMAND_SET:
		{
			if(Rn2903CurrentCommand.commandInfo.radioSetType < RN2903_RADIO_SET_NUMBER)
			{ 
				(*rn2903_radioSetFunctions[Rn2903CurrentCommand.commandInfo.radioSetType])();
			} else {
				rn2903_resetCurrentCommand();
			}
			break;
		}
		
		case RN2903_RADIO_COMMAND_GET:
		{
			if(Rn2903CurrentCommand.commandInfo.radioGetType < RN2903_RADIO_GET_NUMBER)
			{ 
				(*rn2903_radioGetFunctions[Rn2903CurrentCommand.commandInfo.radioGetType])();
			} else {
				rn2903_resetCurrentCommand();
			}
			break;
		}
		
		default:
		{
			rn2903_resetCurrentCommand();
			break;
		}
	}
}

// rn2903Handler_commandNoArguments
// Handles commands that have no arguments to be sent with normal command.
// Acknowledgement comes from rn2903Handler_receivedCallback.
// Inputs: How long to wait for response in ms. (0 means do not wait).
// Outputs: None
static void rn2903Handler_commandNoArguments(char* preamble, char* cmd, unsigned long timeoutMs) {
	char packet[RN2903_MAX_BUFFER_SIZE];
	unsigned char packetLength = 0;
	unsigned char preambleLength = strlen(preamble);
	unsigned char cmdLength = strlen(cmd);
	
	// Build the packet
	strncat(packet, preamble, preambleLength);
	strncat(packet, " ", 1);
	strncat(packet, cmd, cmdLength);
	strncat(packet, "/r/n", 2);
	packetLength = preambleLength + cmdLength + 3; // the three is for a space and /r/n.
	rn2903Handler_handleGetCommands(packet, packetLength, timeoutMs);
}

// rn2903Handler_receivedCallback
// Callback when data is received from rn2903 module.
// Must have an active command going or data is thrown out.
// Inputs: The received data as an array and length of array.
// Outputs: None
static void rn2903Handler_receivedCallback(char* data, unsigned short length) {
	if(Rn2903HandlerInit != RN2903_H_NOT_INIT) {
		if(Rn2903CurrentCommand.commandInfo.commandType != RN2903_COMMAND_UNKNOWN) {
			struct rn2903_data_type returnData;
			Rn2903AcknowledgementStatus = RN2903_H_COMMAND_ACKNOWLEDGED;
			returnData.commandInfo = Rn2903CurrentCommand.commandInfo;
			
			if(!strncmp(data, Rn2903InvalidResponse, 13)) {
				strcpy(returnData.response, RN2903_ERROR_MESSAGE);
				returnData.responseLength = strlen(RN2903_ERROR_MESSAGE);
			} else {
				memcpy(returnData.response, data, length);
				returnData.responseLength = length;
			}
			
			if(Rn2903CurrentCommand.callback != RN2903_NO_CALLBACK) {
				Rn2903CurrentCommand.callback(returnData);
			}
		}
	}
}

static void rn2903_handleSysSetNvm(void) {
	
}

static void rn2903_handleSysSetPinDig(void) {
	
}
static void rn2903_handleSysSetPinMode(void) {
	
}

// rn2903_handleSysGetVer
// Puts together command to send to rn2903 module.
// Should only be called by the system handler function.
// Inputs: None
// Outputs: None
static void rn2903_handleSysGetVer(void) {
	char command[] = "sys get ver\r\n";
	rn2903Handler_handleGetCommands(command, strlen(command), RN2903_H_DEFAULT_TIMEOUT_MS);
}

static void rn2903_handleSysGetNvm(void) {
	
}

// rn2903_handleSysGetVdd
// Puts together command to send to rn2903 module.
// Should only be called by the system handler function.
// Inputs: None
// Outputs: None
static void rn2903_handleSysGetVdd(void) {
	char command[] = "sys get vdd\r\n";
	rn2903Handler_handleGetCommands(command, strlen(command), RN2903_H_DEFAULT_TIMEOUT_MS);
}

static void rn2903_handleSysGetHweui(void) {
	char command[] = "sys get hweui\r\n";
	rn2903Handler_handleGetCommands(command, strlen(command), RN2903_H_DEFAULT_TIMEOUT_MS);
}

static void rn2903_handleSysGetPinDig(void) {
	
}

static void rn2903_handleSysGetPinAna(void) {
	
}

static void rn2903_handleMacSetDevAddr(void) {
	
}

static void rn2903_handleMacSetDevEui(void) {
	
}

static void rn2903_handleMacSetAppEui(void) {
	
}

static void rn2903_handleMacSetNwkSkey(void) {
	
}

static void rn2903_handleMacSetAppSkey(void) {
	
}

static void rn2903_handleMacSetPwrIdx(void) {
	
}

static void rn2903_handleMacSetDr(void) {
	
}

static void rn2903_handleMacSetAdr(void) {
	
}

static void rn2903_handleMacSetBat(void) {
	
}

static void rn2903_handleMacSetRetx(void) {
	
}

static void rn2903_handleMacSetLinkChk(void) {
	
}

static void rn2903_handleMacSetRxDelay1(void) {
	
}

static void rn2903_handleMacSetAr(void) {
	
}

static void rn2903_handleMacSetRx2(void) {
	
}

static void rn2903_handleMacSetSync(void) {
	
}

static void rn2903_handleMacSetUpCtr(void) {
	
}

static void rn2903_handleMacSetDnCtr(void) {
	
}

static void rn2903_handleMacSetCh(void) {
	
}

static void rn2903_handleMacGetDevAddr(void) {
	
}

static void rn2903_handleMacGetDevEui(void) {
	
}

static void rn2903_handleMacGetAppEui(void) {
	
}

static void rn2903_handleMacGetDr(void) {
	
}

static void rn2903_handleMacGetPwrIdx(void) {
	
}

static void rn2903_handleMacGetAdr(void) {
	
}

static void rn2903_handleMacGetRetx(void) {
	
}

static void rn2903_handleMacGetRxDelay1(void) {
	
}

static void rn2903_handleMacGetRxDelay2(void) {
	
}

static void rn2903_handleMacGetAr(void) {
	
}

static void rn2903_handleMacGetRx2(void) {
	
}

static void rn2903_handleMacGetDCyclePs(void) {
	
}

static void rn2903_handleMacGetMrgn(void) {
	
}

static void rn2903_handleMacGetGwNb(void) {
	
}

static void rn2903_handleMacGetStatus(void) {
	
}

static void rn2903_handleMacGetSync(void) {
	
}

static void rn2903_handleMacGetUpCtr(void) {
	
}

static void rn2903_handleMacGetDnCtr(void) {
	
}

static void rn2903_handleMacGetCh(void) {
	
}

static void rn2903_handleRadioSetBt(void) {
	
}

static void rn2903_handleRadioSetMod(void) {
	
}

static void rn2903_handleRadioSetFreq(void) {
	
}

static void rn2903_handleRadioSetPwr(void) {
	
}

static void rn2903_handleRadioSetSf(void) {
	
}

static void rn2903_handleRadioSetAfcBw(void) {
	
}

static void rn2903_handleRadioSetRxBw(void) {
	
}

static void rn2903_handleRadioSetBitrate(void) {
	
}

static void rn2903_handleRadioSetFdev(void) {
	
}

static void rn2903_handleRadioSetPrLen(void) {
	
}

static void rn2903_handleRadioSetCrc(void) {
	
}

static void rn2903_handleRadioSetIqi(void) {
	
}

static void rn2903_handleRadioSetCr(void) {
	
}

static void rn2903_handleRadioSetWdt(void) {
	
}

static void rn2903_handleRadioSetSync(void) {
	
}

static void rn2903_handleRadioSetBw(void) {
	
}

static void rn2903_handleRadioGetBt(void) {
	
}

static void rn2903_handleRadioGetMod(void) {
	
}

static void rn2903_handleRadioGetFreq(void) {
	
}

static void rn2903_handleRadioGetPwr(void) {
	
}

static void rn2903_handleRadioGetSf(void) {
	
}

static void rn2903_handleRadioGetAfcBw(void) {
	
}

static void rn2903_handleRadioGetRxBw(void) {
	
}

static void rn2903_handleRadioGetBitrate(void) {
	
}

static void rn2903_handleRadioGetFdev(void) {
	
}

static void rn2903_handleRadioGetPrLen(void) {
	
}

static void rn2903_handleRadioGetCrc(void) {
	
}

static void rn2903_handleRadioGetIqi(void) {
	
}

static void rn2903_handleRadioGetCr(void) {
	
}

static void rn2903_handleRadioGetWdt(void) {
	
}

static void rn2903_handleRadioGetBw(void) {
	
}

static void rn2903_handleRadioGetSnr(void) {
	
}

static void rn2903_handleRadioGetSync(void) {
	
}

// rn2903Handler_handleGetCommands
// Handles get command state machine.
// Resets current command when finished.
// Inputs: Fully constructed packet, length of packet, and how long to wait for response in ms (0 for no wait).
// Outputs: None
static void rn2903Handler_handleGetCommands(char* packet, unsigned char packetLength, unsigned long timeoutMs) {
	switch(Rn2903GetDataState) {
		case RN2903_H_GET_DATA_STATE_TX:
		{
			char printBuffer[128];
			Rn2903_SendCommand(packet, packetLength);
			
			packet[packetLength-2] = 0;
			sprintf(printBuffer, "Rn2903 Handler: Sent - %s" , packet);
			Console_DebugPrint(printBuffer);
			
			Rn2903AcknowledgementStatus = RN2903_H_COMMAND_NOT_ACKNOWLEDGED;
			Rn2903GetDataState = RN2903_H_GET_DATA_STATE_WAIT_FOR_RX;
			Timer1ms_Set(TIMER1MS_TIMER_RN2903_RX_WAIT, 0);
			
			if(timeoutMs == RN2903_H_HAS_NO_RESPONSE) {
				Console_DebugPrint("Rn2903 Handler: No response needed.\n");
				rn2903_resetCurrentCommand();
			} else {
				Rn2903GetDataState = RN2903_H_GET_DATA_STATE_WAIT_FOR_RX;
			}
			break;
		}
		
		case RN2903_H_GET_DATA_STATE_WAIT_FOR_RX:
		{
			if(Rn2903AcknowledgementStatus == RN2903_H_COMMAND_ACKNOWLEDGED) {
				Console_DebugPrint("Rn2903 Handler: Received Response\n");
				Rn2903GetDataState = RN2903_H_GET_DATA_STATE_TX;
				rn2903_resetCurrentCommand();
			}
			
			if(Timer1ms_Get(TIMER1MS_TIMER_RN2903_RX_WAIT) >= timeoutMs) {
				Console_DebugPrint("Rn2903 Handler: Response timed out.\n");
				Rn2903GetDataState = RN2903_H_GET_DATA_STATE_TX;
				rn2903_resetCurrentCommand();
			}
			break;
		}
	}
}
