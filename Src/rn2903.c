// rn2903.c
// Controls sending and receiving from the RN2903 Microchip LoRa Module.
// There are 2 different radio modes: LoRaWAN and LoRa.
// LoRa mode allows access to the low level registers and allows creation of propriatory networks.
// LoRaWAN mode configures the device to communicate to LoRaWAN gateways.
// Author: Bjotech
// Contributors: Bryan Brumm

#include "gpio.h"
#include "string.h"
#include "rn2903.h"
#include "usart.h"

#define RN2903_NOT_INIT	(0)
#define RN2903_INIT			(1)

#define RN2903_END_OF_RESPONSE_LENGTH	(2)

static unsigned char Rn2903Init = RN2903_NOT_INIT;
static unsigned char Rn2903RxBuffer[RN2903_MAX_BUFFER_SIZE];
static unsigned short Rn2903RxBufferLength = 0;

static void rn2903_resetRxBuffer(void);
static void (*rn2903_rxCallback)(char* returnData, unsigned short length);

// Rn2903_Init
// Initializes communication and buffers for rn2903.
// Inputs: None
// Outputs: None
void Rn2903_Init(void) {
	if(Rn2903Init == RN2903_NOT_INIT) {
		GPIO_SetLoraReset();
		HAL_UART_MspInit(&huart1);
		rn2903_rxCallback = RN2903_NO_CALLBACK;
		Rn2903Init = RN2903_INIT;
	}
}

// Rn2903_Process
// Controls the state machine for the RN2903 module.
// Inputs: None
// Outputs: Can fail if not initialized.
RN2903_STATUS_TYPE Rn2903_Process(void) {
	if(Rn2903Init == RN2903_NOT_INIT) {
		return RN2903_STATUS_NOT_INIT;
	}
	
	while(Uart_CheckRn2903Buffer(&Rn2903RxBuffer[Rn2903RxBufferLength]) == UART_BUFFER_PASSED) {	
		Rn2903RxBufferLength++;
		
		// <cr><lf> determines the end of command.
		if(Rn2903RxBufferLength >= RN2903_END_OF_RESPONSE_LENGTH) {
			if((Rn2903RxBuffer[Rn2903RxBufferLength-1] == '\n') && (Rn2903RxBuffer[Rn2903RxBufferLength-2] == '\r')) {
				if(rn2903_rxCallback != RN2903_NO_CALLBACK) {
					rn2903_rxCallback((char*)Rn2903RxBuffer, Rn2903RxBufferLength);
				}
				rn2903_resetRxBuffer();
			}
		}
		
		if(Rn2903RxBufferLength >= RN2903_MAX_BUFFER_SIZE) {
			rn2903_resetRxBuffer();
		}
	}
	
	return RN2903_STATUS_SUCCESS;
}

// Rn2903_QueueCommand
// Queues command to be sent out to RN2903.
// Inputs: Pointer to command data to be sent.
// Outputs: Can fail if rn2903 not initialized or command buffer is full.
RN2903_STATUS_TYPE Rn2903_SendCommand(char* data, unsigned short length) {
	if(Rn2903Init == RN2903_NOT_INIT) {
		return RN2903_STATUS_NOT_INIT;
	}
	
	Uart_SendRn2903Data((unsigned char*)data, length);
	return RN2903_STATUS_SUCCESS;
}

// Rn2903_SetupRxCallback
// Sets up Rx callback for when a general response is received from rn2903.
// Inputs: Pointer to the callback function.
// Outputs: Can fail if rn2903 has not been initialized yet.
RN2903_STATUS_TYPE Rn2903_SetupRxCallback(void(*rxCallback)(char* returnData, unsigned short length)) {
	if(Rn2903Init == RN2903_NOT_INIT) {
		return RN2903_STATUS_NOT_INIT;
	}
	
	rn2903_rxCallback = rxCallback;
	return RN2903_STATUS_SUCCESS;
}

// rn2903_resetRxBuffer
// Resets counter for rx buffer.
// Inputs: None
// Outputs: None
static void rn2903_resetRxBuffer(void) {
	memset(Rn2903RxBuffer, 0, Rn2903RxBufferLength);
	Rn2903RxBufferLength = 0;
}
