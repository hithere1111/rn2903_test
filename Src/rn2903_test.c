// rn2903_test.h
// Provides test functions for rn2903 and shows how to make
// calls to the rn2903 module.
// Author: Bjotech
// Contributors: Bryan Brumm

#include "console.h"
#include "rn2903_handler.h"
#include "rn2903_test.h"
#include "string.h"
#include "cstdio"

#define RN2903_T_NOT_INIT	(0)
#define RN2903_T_INIT			(1)

#define RN2903_TEST_PENDING	(0)
#define RN2903_TEST_PASSED	(1)
#define RN2903_TEST_FAILED	(2)
#define RN2903_NUMBER_OF_TEST_STATUS	(3)

typedef enum {
	RN2903_T_STATE_INIT,
	RN2903_T_STATE_SYSTEM_TESTS,
	RN2903_T_STATE_MAC_TESTS,
	RN2903_T_STATE_RADIO_TESTS,
	RN2903_T_STATE_FINISHED,
	RN2903_T_STATE_PASSED,
	RN2903_T_STATE_FAILED
} RN2903_T_STATE_TYPE;

typedef enum {
	RN2903_T_SYSTEM_TEST_GET_VERSION,
	RN2903_T_SYSTEM_TEST_GET_VOLTAGE,
	RN2903_T_SYSTEM_TEST_GET_HW_EUI,
	RN2903_T_SYSTEM_TEST_NUMBER
} RN2903_T_SYSTEM_TEST_TYPE;

typedef enum {
	RN2903_T_SYSTEM_STATE_QUEUE_COMMANDS,
	RN2903_T_SYSTEM_STATE_WAIT_FOR_RESPONSES,
} RN2903_T_SYSTEM_STATE_TYPE;

static unsigned char Rn2903SystemTests[RN2903_T_SYSTEM_TEST_NUMBER] = 
{
	RN2903_TEST_PENDING,
	RN2903_TEST_PENDING,
	RN2903_TEST_PENDING
};

static char* Rn2903TestPrintStatus[RN2903_NUMBER_OF_TEST_STATUS] = 
{
	"Pending",
	"Passed",
	"Failed"
};

static char* Rn2903SystemPrintLabels[RN2903_T_SYSTEM_TEST_NUMBER] =
{
	"System Get Model Version:",
	"System Get Voltage:",
	"System Get HW EUI:",
};

static unsigned char Rn2903TestInit = RN2903_T_NOT_INIT;
static RN2903_T_STATE_TYPE Rn2903TestState = RN2903_T_STATE_INIT;
static RN2903_T_SYSTEM_STATE_TYPE Rn2903SystemTestState = RN2903_T_SYSTEM_STATE_QUEUE_COMMANDS;

static RN2903_T_STATUS_TYPE rn2903Test_handleSystemTests(void);
static void rn2903Test_systemGetVersionResponse(struct rn2903_data_type data);
static void rn2903Test_systemGetVoltageResponse(struct rn2903_data_type data);
static void rn2903Test_systemGetHwEuiResponse(struct rn2903_data_type data);
static RN2903_T_STATUS_TYPE rn2903Test_checkSystemTests(void);
static RN2903_T_STATUS_TYPE rn2903Test_checkTests(void);

// Rn2903Test_Init
// Initializes the rn2903 handler for testing and
// resets the test variables.
// Inputs: None
// Outputs: None
void Rn2903Test_Init(void) {
	if(Rn2903TestInit == RN2903_T_NOT_INIT) {
		Rn2903Handler_Init();
		Rn2903TestInit = RN2903_T_INIT;
	}
}

// Rn2903Test_Process
// Handles state machine for all the tests: 
// System, Mac, and Radio.
// Inputs: None
// Outputs: Can fail if not initialized. Will return success or failure
// 	once finished.
RN2903_T_STATUS_TYPE Rn2903Test_Process(void) {
	if(Rn2903TestInit == RN2903_T_NOT_INIT) {
		return RN2903_T_STATUS_FAILURE;
	}
	
	Rn2903Handler_Process();
	
	switch(Rn2903TestState) {
		case RN2903_T_STATE_INIT:
		{
			Rn2903TestState = RN2903_T_STATE_SYSTEM_TESTS;
			break;
		}
		
		case RN2903_T_STATE_SYSTEM_TESTS:
		{
			if(rn2903Test_handleSystemTests() != RN2903_T_STATUS_TEST_IN_PROGRESS) {
				Rn2903TestState = RN2903_T_STATE_MAC_TESTS;
			}
			break;
		}
		
		case RN2903_T_STATE_MAC_TESTS:
		{
			Rn2903TestState = RN2903_T_STATE_RADIO_TESTS;
			break;
		}
		
		case RN2903_T_STATE_RADIO_TESTS:
		{
			Rn2903TestState = RN2903_T_STATE_FINISHED;
			break;
		}
		
		case RN2903_T_STATE_FINISHED:
		{
			if(rn2903Test_checkTests() == RN2903_T_STATUS_SUCCESS) {
				Rn2903TestState = RN2903_T_STATE_PASSED;
			} else {
				Rn2903TestState = RN2903_T_STATE_FAILED;
			}
			
			Rn2903Test_PrintResults();
			break;
		}
		
		case RN2903_T_STATE_PASSED:
		{
			return RN2903_T_STATUS_SUCCESS;
		}
		
		case RN2903_T_STATE_FAILED:
		{
			return RN2903_T_STATUS_FAILURE;
		}
		
		default:
		{
			return RN2903_T_STATUS_FAILURE;
		}
	}
	
	return RN2903_T_STATUS_TEST_IN_PROGRESS;
}

// Rn2903Test_PrintResults
// Prints the current results of test to console.
// Console debug must be on. Blocks until finished.
// Inputs: None
// Outputs: None
void Rn2903Test_PrintResults(void) {
	if(Rn2903TestInit == RN2903_T_INIT) {
		unsigned char i = 0;
		char buffer[128];
		Console_Print("\n======== RN2903 SYSTEM TESTS ========");
		for(i=0; i<RN2903_T_SYSTEM_TEST_NUMBER; i++) {
			sprintf(buffer, "%-30s %s", Rn2903SystemPrintLabels[i], Rn2903TestPrintStatus[Rn2903SystemTests[i]]);
			Console_Print(buffer);
		}
		Console_Print("=====================================");
	}
}

// rn2903Test_handleSystemTests
// Handles state machine for all system tests.
// System tests include getting system information.
// Inputs: None
// Outputs: Will send success once finished with system tests.
static RN2903_T_STATUS_TYPE rn2903Test_handleSystemTests(void) {
	switch(Rn2903SystemTestState) {
		case RN2903_T_SYSTEM_STATE_QUEUE_COMMANDS:
		{
			struct rn2903_command_type command;
			command.commandInfo.commandType = RN2903_COMMAND_SYSTEM;
			command.commandInfo.sysType = RN2903_SYSTEM_COMMAND_GET;
			
			// Queue version command.
			command.commandInfo.sysGetType = RN2903_SYSTEM_GET_VER;
			command.callback = rn2903Test_systemGetVersionResponse;
			Rn2903Handler_QueueCommand(command);
			
			// Queue voltage command.
			command.commandInfo.sysGetType = RN2903_SYSTEM_GET_VDD;
			command.callback = rn2903Test_systemGetVoltageResponse;
			Rn2903Handler_QueueCommand(command);
			
			// Queue hw eui command.
			command.commandInfo.sysGetType = RN2903_SYSTEM_GET_HW_EUI;
			command.callback = rn2903Test_systemGetHwEuiResponse;
			Rn2903Handler_QueueCommand(command);
			
			Rn2903SystemTestState = RN2903_T_SYSTEM_STATE_WAIT_FOR_RESPONSES;
			break;
		}
		
		case RN2903_T_SYSTEM_STATE_WAIT_FOR_RESPONSES:
		{
			return rn2903Test_checkSystemTests();
		}
	}
	
	return RN2903_T_STATUS_TEST_IN_PROGRESS;
}

// rn2903Test_systemGetVersionResponse
// Callback for getting version Id.
// Inputs: None
// Outputs: None
static void rn2903Test_systemGetVersionResponse(struct rn2903_data_type data) {
	if(strncmp((char*)data.response, RN2903_ERROR_MESSAGE, RN2903_ERROR_MESSAGE_LENGTH) == 0) {
		Rn2903SystemTests[RN2903_T_SYSTEM_TEST_GET_VERSION] = RN2903_TEST_FAILED;
	} else {
		Rn2903SystemTests[RN2903_T_SYSTEM_TEST_GET_VERSION] = RN2903_TEST_PASSED;
	}
}

// rn2903Test_systemGetVoltageResponse
// Callback for getting system voltage.
// Inputs: None
// Outputs: None
static void rn2903Test_systemGetVoltageResponse(struct rn2903_data_type data) {
	if(strncmp((char*)data.response, RN2903_ERROR_MESSAGE, RN2903_ERROR_MESSAGE_LENGTH) == 0) {
		Rn2903SystemTests[RN2903_T_SYSTEM_TEST_GET_VOLTAGE] = RN2903_TEST_FAILED;
	} else {
		Rn2903SystemTests[RN2903_T_SYSTEM_TEST_GET_VOLTAGE] = RN2903_TEST_PASSED;
	}
}

// rn2903Test_systemGetHwEuiResponse
// Callback for getting Hw Eui.
// Inputs: None
// Outputs: None
static void rn2903Test_systemGetHwEuiResponse(struct rn2903_data_type data) {
	if(strncmp((char*)data.response, RN2903_ERROR_MESSAGE, RN2903_ERROR_MESSAGE_LENGTH) == 0) {
		Rn2903SystemTests[RN2903_T_SYSTEM_TEST_GET_HW_EUI] = RN2903_TEST_FAILED;
	} else {
		Rn2903SystemTests[RN2903_T_SYSTEM_TEST_GET_HW_EUI] = RN2903_TEST_PASSED;
	}
}

// rn2903Test_checkSystemTest
// Checks if there are any pending tests.
// Inputs: None
// Outputs: Test in progress if any tests are still pending.
//		Otherwise, success or failure based on if a test failed.
static RN2903_T_STATUS_TYPE rn2903Test_checkSystemTests(void) {
	unsigned char i = 0;
	unsigned char testStatus = RN2903_TEST_PASSED;
	for(i=0; i < RN2903_T_SYSTEM_TEST_NUMBER; i++) {
		if(Rn2903SystemTests[i] == RN2903_TEST_FAILED) {
			testStatus = RN2903_TEST_FAILED;
		}
		
		if(Rn2903SystemTests[i] == RN2903_TEST_PENDING) {
			return RN2903_T_STATUS_TEST_IN_PROGRESS;
		}
	}
	
	if(testStatus == RN2903_TEST_FAILED) {
		return RN2903_T_STATUS_FAILURE;
	} else {
		return RN2903_T_STATUS_SUCCESS;
	}
}

// rn2903Test_checkTests
// Checks to see if all tests passed or failed.
// If one fails return a failure status.
// Inputs: None
// Outputs: Can fail if a test fails.
static RN2903_T_STATUS_TYPE rn2903Test_checkTests(void) {
	RN2903_T_STATUS_TYPE systemTestStatus = rn2903Test_checkSystemTests();
	RN2903_T_STATUS_TYPE macTestStatus = RN2903_T_STATUS_SUCCESS;
	RN2903_T_STATUS_TYPE radioTestStatus = RN2903_T_STATUS_SUCCESS;
	
	if( (systemTestStatus == RN2903_T_STATUS_TEST_IN_PROGRESS) 	||
			(macTestStatus == RN2903_T_STATUS_TEST_IN_PROGRESS)			||
			(radioTestStatus == RN2903_T_STATUS_TEST_IN_PROGRESS) ) {
				return RN2903_T_STATUS_TEST_IN_PROGRESS;
	}
			
	if( (systemTestStatus == RN2903_T_STATUS_FAILURE) 	||
			(macTestStatus == RN2903_T_STATUS_FAILURE)			||
			(radioTestStatus == RN2903_T_STATUS_FAILURE) ) {
				return RN2903_T_STATUS_FAILURE;
	}
	
	return RN2903_T_STATUS_SUCCESS;
}
