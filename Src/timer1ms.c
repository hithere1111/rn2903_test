// timer1ms.c
// Controls a timer based interrupt for 1ms timer variables.
// Interrupt is called every 1ms, and variables defined in the header are incremented 1.
// Note that these variables are approximate in ms. They will be off by a few microcseconds.
// Author: Bjotech
// Contributors: Bryan Brumm

#include "timer1ms.h"

#define TIMER1MS_NOT_INIT	(0)
#define TIMER1MS_INIT			(1)

static unsigned char Timer1msInit = TIMER1MS_NOT_INIT;
static unsigned long Timer1msTimers[NUMBER_OF_TIMER1MS_TIMERS];

// Timer1ms_Init
// Sets all timer variables to 0 and starts timer interrupts.
// Inputs: Pointer to timer handler to start.
// Outputs: Currenly always returns success.
TIMER1MS_STATUS_TYPE Timer1ms_Init(TIM_HandleTypeDef* htim) {
	if(Timer1msInit == TIMER1MS_NOT_INIT) {
		unsigned char i = 0;
		
		for(i=0; i<NUMBER_OF_TIMER1MS_TIMERS; i++) {
			Timer1msTimers[i] = 0;
		}
		
		Timer1msInit = TIMER1MS_INIT;
		HAL_TIM_Base_Start_IT(htim);
	}
	
	return TIMER1MS_STATUS_SUCCESS;
}

// Timer1ms_Get
// Gets a specified 1ms timer based on variable.
// Inputs: The timer to get and a time container to hold the time.
// Outputs: Can fail if timer not init or timer provided is greater than # of timers.
unsigned long Timer1ms_Get(TIMER1MS_TIMER_TYPE timer) {
	if(Timer1msInit == TIMER1MS_NOT_INIT) {
		return TIMER1MS_STATUS_FAILURE;
	}
	
	if(timer >= NUMBER_OF_TIMER1MS_TIMERS) {
		return TIMER1MS_STATUS_FAILURE;
	}
	
	return Timer1msTimers[timer];
}

// Timer1ms_Set
// Sets a specified 1ms timer based on variable.
// Inputs: The timer to update and the time to set it to in milliseconds.
// Outputs: Can fail if timer not init or timer provided is greater than # of timers.
TIMER1MS_STATUS_TYPE Timer1ms_Set(TIMER1MS_TIMER_TYPE timer, unsigned long time) {
	if(Timer1msInit == TIMER1MS_NOT_INIT) {
		return TIMER1MS_STATUS_FAILURE;
	}
	
	if(timer >= NUMBER_OF_TIMER1MS_TIMERS) {
		return TIMER1MS_STATUS_FAILURE;
	}
	
	Timer1msTimers[timer] = time;
	return TIMER1MS_STATUS_SUCCESS;
}

// Timer1ms_Update
// Is called by a 1ms timer interrupt. Updates all timer variables by 1.
// Inputs: None
// Outputs: None
void Timer1ms_Update(void) {
	if(Timer1msInit == TIMER1MS_INIT) {
		unsigned char i = 0;
		for(i=0; i<NUMBER_OF_TIMER1MS_TIMERS; i++) {
			Timer1msTimers[i]++;
		}
	}
}
