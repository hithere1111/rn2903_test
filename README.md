rn2903_test
+Tests the features of the Microchip RN2903 LoRa module. 
+Will contain functions to allow communication via a LoRa private network or LoRaWAN network. 
+Can be used as a starting point for local long range networks for IoT applications.
+Utilizes the mikroe-2225 click board and stm32f072b discovery evaluation board.

CONNECTIONS (Reference mikroe-2225_pinout.png for mikroe-2225 pin mapping)
+USB from STM to 5 volt power source
+3 volt from STM to pin 7 of mikroe-2225 board
+GND of stm to pin 8 and 9 of mikroe-2225 board
+PA9 Uart1_Tx of STM connects to pin 13 of mikroe-2225 board
+PA10 Uart1_Rx of STM connects to pin 14 of mikroe-2225 board
+PA15 Active Low Rest of STM connects to pin 2 of mikroe-2225 board
+Make sure power jumper for mikroe-2225 is set for 3.3 volt

